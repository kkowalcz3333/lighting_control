from machine import Pin, PWM
from time import sleep

# ilość poziomów naświetlenia
MAX_LIGHT_STAGE = 3

# dostępne piny
PINS = [2, 4, 5, 12, 13, 14, 15, 16, 17, 18, 19,
        21, 22, 23, 25, 26, 27, 32, 33]


class Light:
    """
    Klasa obiektów do sterowania oświetleniem. Ma atrybuty:

    :param int pin: Numer pinu, do którego jest podłączone dane urządzenie.

    :param bool dimmable: Informacja, czy światło należy przyciemniać.
                          0 - wcale, 1 - migając, 2 - płynnie.

    :param handle: Zmienna dostępu do urządzenia sterującego światłem.
                   Jest to PWM, jeżeli dimmable == True
                   i Pin, jeżeli dimmable = false.
    """
    def __init__(self, pin: int, dimmable: int):
        if pin not in PINS:
            raise Exception("Niewlasciwy pin")
        self.pin = pin
        self.dimmable = dimmable
        if dimmable == 2:
            self.handle = PWM(Pin(pin))
            # dotyczy diody, implementacja dimmera może wyglądać inaczej
        else:
            self.handle = Pin(pin, Pin.OUT)


def load_lights(taken_pins: list[int]) -> list[Light]:
    """
    Wczytuje z pliku piny i typy oświetlenia.
    Zwraca listę urządzeń jako obiekty typu Light.
    Wyrzuca wyjątek, jeżeli pin był już użyty.
    """
    lights = []
    with open("mp_data.txt", "r") as file:
        for _ in range(6):
            data = file.readline()
        while data:
            data = data.strip().upper().split(",")
            try:
                pin = int(data[0])
                dimmable = int(data[1])
            except ValueError:
                raise Exception("Niewlasciwie zapisane dane")
            if pin in taken_pins:
                raise Exception("Pin", pin, "zostal juz uzyty")
            taken_pins.append(pin)

            lights.append(Light(pin, dimmable))
            data = file.readline()
    return lights


def light_lights(lights: list[Light]) -> None:
    """
    Natychmiastowo zapala wszystkie światła naraz.
    """
    for light in lights:
        if light.dimmable == 2:
            light.handle.duty(1023)
        else:
            light.handle.on()
    set_light_stage(MAX_LIGHT_STAGE)


def disable_lights(lights: list[Light]) -> None:
    """
    Natychmiastowo gasi wszystkie światła naraz.
    """
    for light in lights:
        if light.dimmable == 2:
            light.handle.duty(0)
        else:
            light.handle.off()
    set_light_stage(0)


def get_light_stage() -> int:
    """
    Pobiera z pliku informację o poziomie naświetlenia pomieszczenia.
    """
    with open("mp_light_stage.txt", "r") as file:
        result = int(file.readline().strip())
    return result


def set_light_stage(stage: int) -> None:
    """
    Ustawia w pliku zadany poziom naświetlenia pomieszczenia.
    """
    with open("mp_light_stage.txt", "w") as file:
        file.write(str(stage))


def dim_transition(lights: list[Light], fraction: int,
                   initial_duty: int, decrement: int) -> None:
    """
    Zmienia stopień naświetlenia pomieszczenia, przyciemniając światła
    lub migając nimi i gasząc część z nich.
    """
    duty = initial_duty
    value = 1
    for _ in range(5):
        non_dimmable_count = 0
        value = 0 if value == 1 else 1
        duty -= decrement
        for light in lights:
            if light.dimmable == 2:
                light.handle.duty(duty)
            elif light.dimmable == 1:
                # 1/{fraction} zwykłych świateł ciągle świeci, reszta miga
                if non_dimmable_count % fraction != 0:
                    light.handle.value(value)
                non_dimmable_count += 1
        sleep(0.25)
        duty -= decrement
        for light in lights:
            if light.dimmable == 2:
                light.handle.duty(duty)
        sleep(0.25)


def dim_lights(lights: list[Light]) -> None:
    """
    Przyciemnia światła o jeden stopień.
    """
    stage = get_light_stage()
    if stage == 0:
        return
    stage -= 1
    set_light_stage(stage)

    if stage == 2:
        dim_transition(lights, 2, 1023, 82)
    elif stage == 1:
        dim_transition(lights, 4, 200, 15)
    else:
        dim_transition(lights, 4, 50, 5)
        disable_lights(lights)
