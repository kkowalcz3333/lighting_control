import time
from mp_lights import load_lights, light_lights, dim_lights
from machine import Pin # to zadziała tylko z MicroPythonem
from hcsr04 import HCSR04
# Pin od przycisku
BUTTON_PIN = 26

# Piny od czujnika odległości
TRIGGER_PIN = 14
SENSOR_PIN = 13

# Piny od czujnika ruchu
MOTION_PIN = 33

# Wszystkie dostępne piny
PINS = [2, 4, 5, 12, 13, 14, 15, 16, 17, 18, 19,
        21, 22, 23, 25, 26, 27, 32, 33]

# Zmienna przechowująca stan czujnika
motion = False


def handle_interrupt(pin):
    """
    Funkcja odpowiedzialna za zmianę stanu czujnika po wykryciu ruchu.
    """
    global motion
    motion = True


def get_hours_days() -> list[list[int]]:
    """
    Wczytuje z pliku i zwraca (lista numerów godzin/dni):
    - godziny intensywnego użytkowania biura
    - godziny zmniejszonego użytkowania biura
    - godziny zamknięcia biura
    - dni, kiedy biuro jest zamknięte (0 - poniedziałek, 6 - sobota)
    """
    result = []
    with open("mp_data.txt", "r") as file:
        for _ in range(4):
            line = file.readline().strip().split(",")
            if line == ['']:
                result.append([])
            else:
                result.append([int(value) for value in line])
    return result


def main_motion():  # wersja z czujnikiem ruchu i diodami
    sensor = Pin(MOTION_PIN, Pin.IN)
    button = Pin(BUTTON_PIN, Pin.IN, Pin.PULL_UP)
    lights = load_lights([BUTTON_PIN, MOTION_PIN])

    global motion
    sensor.irq(trigger=Pin.IRQ_RISING, handler=handle_interrupt)
    shutdown_time = time.time()

    intensive, limited, closed, days = get_hours_days()
    while True:
        time.sleep(0.1)
        current_time = time.time()
        hour = time.localtime()[3]
        weekday = time.localtime()[6]
        if motion or button.value() == 0:
            motion = False
            light_lights(lights)
            if hour in closed or weekday in days:
                shutdown_time = current_time + 10
                # intruder_alert()
            elif hour in intensive:
                shutdown_time = current_time + 5
            elif hour in limited:
                shutdown_time = current_time + 3
        if shutdown_time < current_time:
            dim_lights(lights)
            current_time = time.time()
            shutdown_time = current_time + 3


def main_distance():  # wersja z czujnikiem odległości i diodami
    sensor = HCSR04(trigger_pin=TRIGGER_PIN,
                    echo_pin=SENSOR_PIN,
                    echo_timeout_us=10000)
    button = Pin(BUTTON_PIN, Pin.IN, Pin.PULL_UP)
    lights = load_lights([BUTTON_PIN, TRIGGER_PIN, SENSOR_PIN])

    old_distance = None
    new_distance = sensor.distance_cm()
    shutdown_time = time.time()

    intensive, limited, closed, days = get_hours_days()
    while True:
        time.sleep(0.1)
        old_distance = new_distance
        new_distance = sensor.distance_cm()
        current_time = time.time()
        hour = time.localtime()[3]
        weekday = time.localtime()[6]
        if abs(new_distance - old_distance) > 3 or button.value() == 0:
            light_lights(lights)
            if hour in closed or weekday in days:
                shutdown_time = current_time + 10
                # intruder_alert()
            elif hour in intensive:
                shutdown_time = current_time + 5
            elif hour in limited:
                shutdown_time = current_time + 3
        if shutdown_time < current_time:
            dim_lights(lights)
            current_time = time.time()
            shutdown_time = current_time + 3


if __name__ == "__main__":
    main_distance()
